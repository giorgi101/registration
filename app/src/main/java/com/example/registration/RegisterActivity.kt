package com.example.registration

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth

class RegisterActivity : AppCompatActivity() {

    private lateinit var email :EditText
    private lateinit var password :EditText
    private lateinit var password2 :EditText
    private lateinit var buttonRegister : Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        init()

        registerListeners()

        }

    private fun init() {
        email = findViewById(R.id.email)
        buttonRegister = findViewById(R.id.buttonRegister)
        password = findViewById(R.id.password)
        password2 = findViewById(R.id.password2)
    }
    private fun registerListeners(){
        buttonRegister.setOnClickListener {
            val email = email.text.toString()
            val password = password.text.toString()
            val password2 = password2.text.toString()

            if(email.isEmpty() || password.isEmpty()){
                Toast.makeText(this, "Empty!", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if(password != password2){
                Toast.makeText(this, "Password does not match!", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if(password.length <= 7){
                Toast.makeText(this, "Password must be at least 8 characters.", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if(password.length >= 8 && !password.isEmpty() && password == password2){
                FirebaseAuth.getInstance()
                    .createUserWithEmailAndPassword(email, password,)
                    .addOnCompleteListener { task ->
                        if(task.isSuccessful){
                            Toast.makeText(this, "Account Created!", Toast.LENGTH_SHORT).show()
                        }else{
                            Toast.makeText(this, "Input a valid Email.", Toast.LENGTH_SHORT).show()
                        }
                    }
            }



        }

    }



}

